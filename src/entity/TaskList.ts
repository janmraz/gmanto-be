import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Project from './Project';
import Task from './Task';
import { check } from 'express-validator';
import User from './User';

@Entity({ name: 'tasklist' })
export default class TaskList {
	@PrimaryGeneratedColumn()
	id: number;

	@Column('text')
	name: string;

	@Column('bool')
	archived: boolean;

	@Column('smallint', { default: 10000 })
	order: number;

	@Column('datetime', { select: false })
	dateCreated: Date;

	@ManyToOne(
		type => User,
		user => user.createdTaskLists,
		{ onDelete: 'CASCADE' }
	)
	creator: User;

	@ManyToOne(
		type => Project,
		project => project.taskLists,
		{ onUpdate: 'CASCADE', onDelete: 'CASCADE' }
	)
	project: Project;

	@OneToMany(
		type => Task,
		task => task.taskList,
		{ eager: true, onUpdate: 'CASCADE', onDelete: 'CASCADE' }
	)
	tasks: Task[];

	static validators = () => [
		check('id').isNumeric(),
		check('archived').isBoolean(),
		check('name')
			.isString()
			.notEmpty(),
	];
}
