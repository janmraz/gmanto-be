import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import TaskList from './TaskList';
import { check } from 'express-validator';
import { ProjectToUser } from './ProjectToUser';

@Entity()
export default class Project {
	@PrimaryGeneratedColumn()
	id: number;

	@Column('text')
	name: string;

	@Column('bool', { default: false })
	archived: boolean;

	@Column('datetime', { select: false })
	dateCreated: Date;

	@OneToMany(
		type => TaskList,
		taskList => taskList.project,
		{ onDelete: 'CASCADE' }
	)
	taskLists: TaskList[];

	@OneToMany(
		type => ProjectToUser,
		user => user.project,
		{
			cascade: true,
			eager: true,
			onDelete: 'CASCADE',
		}
	)
	collaborators: ProjectToUser[];

	tasksOpen: number;

	tasksMine: number;

	static validators = () => [
		check('id').isNumeric(),
		check('archived').isBoolean(),
		check('name')
			.isString()
			.notEmpty(),
	];
}
