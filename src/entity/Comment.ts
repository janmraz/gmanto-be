import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Task from './Task';
import User from './User';
import { check } from 'express-validator';
import Notification from './Notification';

@Entity()
export default class Comment {
	@PrimaryGeneratedColumn()
	id: number;

	@Column('text')
	text: string;

	@Column('datetime')
	dateCreated: Date;

	@Column('datetime')
	dateModified: Date;

	@Column('bool', { default: false })
	infoComment: boolean;

	@ManyToOne(
		type => Task,
		task => task.comments,
		{ onDelete: 'CASCADE' }
	)
	task: Task;

	@ManyToOne(
		type => User,
		task => task.createdComments,
		{ onDelete: 'CASCADE', cascade: true, eager: true }
	)
	creator: User;

	@OneToMany(
		type => Notification,
		notification => notification.comment,
		{ onDelete: 'CASCADE', cascade: true }
	)
	notifications: Notification[];

	seen: boolean;

	static validators = () => [
		check('id').isNumeric(),
		check('text')
			.isString()
			.notEmpty(),
	];
}
