import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import User from './User';
import Comment from './Comment';

@Entity()
export default class Notification {
	@PrimaryGeneratedColumn()
	id: number;

	@ManyToOne(
		type => Comment,
		comment => comment.notifications,
		{ onDelete: 'CASCADE' }
	)
	comment: Comment;

	@ManyToOne(
		type => User,
		user => user.notifications,
		{ cascade: true, onDelete: 'CASCADE' }
	)
	recipient: User;
}
