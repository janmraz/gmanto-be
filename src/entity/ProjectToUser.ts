import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Project from './Project';
import User from './User';

@Entity({ name: 'project_user' })
export class ProjectToUser {
	@PrimaryGeneratedColumn()
	public id: number;

	@Column('smallint', { default: 10000 })
	public order!: number;

	@Column('bool', { default: false })
	public owner: boolean;

	@ManyToOne(
		type => Project,
		post => post.collaborators,
		{ onDelete: 'CASCADE', onUpdate: 'CASCADE' }
	)
	public project: Project;

	@ManyToOne(
		type => User,
		category => category.collaborators,
		{
			cascade: true,
			eager: true,
			onDelete: 'CASCADE',
			onUpdate: 'CASCADE',
		}
	)
	public user: User;
}
