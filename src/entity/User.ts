import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToMany } from 'typeorm';
import Project from './Project';
import Comment from './Comment';
import Task from './Task';
import { check } from 'express-validator';
import Notification from './Notification';
import { ProjectToUser } from './ProjectToUser';
import TaskList from './TaskList';

@Entity()
export default class User {
	@PrimaryGeneratedColumn()
	id: number;

	@Column('text', { select: false })
	googleId: string;

	@Column('text', { select: false })
	token: string;

	@Column('text')
	firstName: string;

	@Column('text')
	lastName: string;

	@Column('text')
	email: string;

	@Column('text', { nullable: true })
	profileImgUrl: string;

	@Column('datetime', { nullable: true })
	dateCreated: Date;

	@Column('bool', { default: false })
	registered: boolean;

	@Column('bool', { default: false })
	darkMode: boolean;

	@Column('bool', { default: false })
	showAllNotification: boolean;

	@Column('bool', { default: false })
	showCommentsOnly: boolean;

	@OneToMany(
		type => ProjectToUser,
		projectToUser => projectToUser.id,
		{ onDelete: 'CASCADE', onUpdate: 'CASCADE' }
	)
	collaborators: ProjectToUser[];

	@ManyToMany(
		type => Task,
		task => task.followers,
		{ onUpdate: 'CASCADE', onDelete: 'CASCADE' }
	)
	following: Task[];

	@OneToMany(
		type => Task,
		task => task.assignee,
		{ onUpdate: 'CASCADE', onDelete: 'CASCADE' }
	)
	assignedToMe: Task[];

	@OneToMany(
		type => Comment,
		comment => comment.creator,
		{ onUpdate: 'CASCADE', onDelete: 'CASCADE' }
	)
	createdComments: Comment[];

	@OneToMany(
		type => Task,
		task => task.creator,
		{ onDelete: 'CASCADE' }
	)
	createdTasks: Task[];

	@OneToMany(
		type => TaskList,
		task => task.creator,
		{ onDelete: 'CASCADE' }
	)
	createdTaskLists: Task[];

	@OneToMany(
		type => Notification,
		notification => notification.recipient,
		{ onDelete: 'CASCADE' }
	)
	notifications: Notification[];

	@Column('bool', { default: true })
	notificationOn: boolean;

	public getFullName = (): string => {
		return this.firstName + ' ' + this.lastName;
	};

	static validators = () => [
		check('id').isNumeric(),
		check('email').isEmail(),
		check('firstName')
			.isString()
			.notEmpty(),
		check('lastName')
			.isString()
			.notEmpty(),
	];
}
