import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import TaskList from './TaskList';
import Comment from './Comment';
import { check } from 'express-validator';
import User from './User';

@Entity()
export default class Task {
	@PrimaryGeneratedColumn()
	id: number;

	@Column('text')
	name: string;

	@Column('bool')
	done: boolean;

	@Column('smallint', { default: 10000 })
	order: number;

	@Column('datetime', { nullable: true })
	dateDue: Date;

	@Column('datetime', { select: false })
	dateCreated: Date;

	@ManyToOne(
		type => User,
		user => user.createdTasks,
		{ onDelete: 'CASCADE' }
	)
	creator: User;

	@ManyToOne(
		type => TaskList,
		taskList => taskList.tasks,
		{ onDelete: 'CASCADE', onUpdate: 'CASCADE', cascade: true }
	)
	taskList: TaskList;

	@ManyToOne(
		type => User,
		user => user.assignedToMe,
		{ onDelete: 'CASCADE', onUpdate: 'CASCADE', cascade: true, eager: true }
	)
	assignee: User;

	@OneToMany(
		type => Comment,
		comment => comment.task,
		{ onDelete: 'CASCADE', cascade: true, eager: true }
	)
	comments: Comment[];

	@ManyToMany(
		type => User,
		user => user.following,
		{
			cascade: true,
			eager: true,
		}
	)
	@JoinTable({ name: 'task-followers_user' })
	followers: User[];

	static validators = () => [
		check('id').isNumeric(),
		check('done').isBoolean(),
		check('name')
			.isString()
			.notEmpty(),
	];
}
