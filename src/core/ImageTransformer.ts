const sharp = require('sharp');

const cropImage = async (path: string): Promise<string> => {
	await sharp(path)
		.resize(400)
		.toFile(path + '.cropped');
	return path + '.cropped';
};

export default cropImage;
