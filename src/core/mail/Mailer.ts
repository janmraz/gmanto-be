import getMailTransporter from './getMailTransporter';
import { Transporter } from 'nodemailer';
import User from '../../entity/User';
import Project from '../../entity/Project';
import Task from '../../entity/Task';
import logger from '../logger';
import config from '../../config';

export default class Mailer {
	private transporter: Transporter;

	init = (name: string, password: string): Mailer => {
		this.transporter = getMailTransporter(name, password);
		return this;
	};

	sendEmail = async (from: string, to: string, subject: string, text: string, html: string) => {
		const info = await this.transporter.sendMail({
			from,
			to,
			subject,
			text,
			html,
		});

		console.log('Message sent: %s to user %s', info.messageId, to);
	};

	sendRequestAccessEmail = async (project: Project, user: User) => {
		const text = user.getFullName() + ' has request access to project ' + project.name;
		const html = text;
		const subject = 'request access';
		const owner = project.collaborators.find(c => c.owner);
		if (owner) {
			await this.sendEmail('gmantonotification@gmail.com', owner.user.email, subject, text, html);
		} else {
			logger.error('no owner of project');
		}
	};

	sendTaskCompletionEmail = async (user: User, whom: User, task: Task, projectId: string) => {
		const text = user.getFullName() + ' has completed task ' + task.name;
		const html = text + this.linkToTask(task, projectId);
		const subject = 'task completed';
		await this.sendEmail('gmantonotification@gmail.com', whom.email, subject, text, html);
	};

	sendMentionsEmail = async (user: User, whom: User, task: Task, projectId: string) => {
		const text = user.getFullName() + ' added comment in task ' + task.name;
		const html = text + this.linkToTask(task, projectId);
		const subject = 'new comment';
		await this.sendEmail('gmantonotification@gmail.com', whom.email, subject, text, html);
	};

	private linkToTask = (task: Task, projectId: string): string => {
		return (
			'<a href="' + config.frontendUrl + '/project-' + projectId + '/task-' + task.id + '" >Show task detail</a>'
		);
	};
}
