const nodemailer = require('nodemailer');

let getMailTransporter = (user: string, pass: string) =>
	nodemailer.createTransport({
		service: 'gmail',
		auth: { user, pass },
		tls: { rejectUnauthorized: false },
	});

export default getMailTransporter;
