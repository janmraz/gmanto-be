import AWS from 'aws-sdk';
import * as fs from 'fs';

const ID = 'AKIAJPBT7EL3JEUNPTSQ';
const SECRET = '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';
const BUCKET_NAME = 'gmanto-s3';

const s3 = new AWS.S3({
	accessKeyId: ID,
	secretAccessKey: SECRET,
});

const uploadFile = async (fileName: string, path: string, projectId: string, taskId: string): Promise<string> => {
	// Read content from the file
	const fileContent = fs.readFileSync(path);

	// Setting up S3 upload parameters
	const params = {
		Bucket: BUCKET_NAME,
		Key: projectId + '/' + taskId + '/' + fileName,
		Body: fileContent,
		ACL: 'public-read',
	};

	try {
		const data = await s3.upload(params).promise();
		console.log(`File uploaded successfully. ${JSON.stringify(data)}`);
		return data.Location;
	} catch (e) {
		console.error(e);
		throw e;
	}
};

export const deleteFile = async (url: string): Promise<boolean> => {

	// Setting up S3 upload parameters
	const params = {
		Bucket: BUCKET_NAME,
		Key: url.split('s3.amazonaws.com/')[1],
	};

	try {
		const data = await s3.deleteObject(params).promise();
		console.log(`File delete successfully. ${JSON.stringify(data)}`);
		return true;
	} catch (e) {
		console.error(e);
		throw e;
	}
};

export default uploadFile;
