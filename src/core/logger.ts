import winston from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';

const myFormat = winston.format.printf(({ level, message, timestamp }) => {
	return `${timestamp} [${level}]: ${message}`;
});

const consoleTransporter = new winston.transports.Console({
	handleExceptions: true,
	level: 'debug',
	format: winston.format.combine(winston.format.colorize(), winston.format.timestamp(), myFormat),
});

const debugDailyRotateFileTransport = new DailyRotateFile({
	filename: 'log/debug-%DATE%.log',
	datePattern: 'YYYY-MM-DD',
	level: 'debug',
	zippedArchive: false,
	maxSize: '100m',
	maxFiles: '14d',
	handleExceptions: false,
});
const jsonDailyRotateFileTransport = new DailyRotateFile({
	filename: 'log/json-%DATE%.log',
	json: true,
	datePattern: 'YYYY-MM-DD',
	zippedArchive: false,
	maxSize: '100m',
	maxFiles: '14d',
	handleExceptions: true,
});
const errorDailyRotateFileTransport = new DailyRotateFile({
	filename: 'log/error-%DATE%.log',
	level: 'error',
	json: true,
	datePattern: 'YYYY-MM-DD',
	zippedArchive: false,
	maxSize: '100m',
	maxFiles: '14d',
	handleExceptions: true,
});

const exceptionHandler = new DailyRotateFile({
	filename: 'log/error-%DATE%.log',
	json: true,
	datePattern: 'YYYY-MM-DD',
	zippedArchive: false,
	maxSize: '100m',
	maxFiles: '14d',
	handleExceptions: true,
});

const logger = winston.createLogger({
	exitOnError: false,
});

logger.add(debugDailyRotateFileTransport);
logger.add(jsonDailyRotateFileTransport);
logger.add(errorDailyRotateFileTransport);
logger.add(consoleTransporter);
logger.exceptions.handle(exceptionHandler);
// const logger = winston.createLogger({
// 	transports: [
// 		new winston.transports.Console(),
// 		new winston.transports.File({ filename: 'combined.log' })
// 	]
// });

export default logger;
