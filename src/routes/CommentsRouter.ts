import { Request } from 'express';
import authCheck from '../auth/authChecker';
import Comment from '../entity/Comment';
import commentService from '../services/CommentService';
import validationMiddleware from '../core/validationMiddleware';
import createDOMPurify from 'dompurify';
import express from 'express';

import { JSDOM } from 'jsdom';
const window = new JSDOM('').window;
const DOMPurify = createDOMPurify(window);

const commentsRouter = express.Router({ mergeParams: true });

commentsRouter.post('/', Comment.validators(), validationMiddleware, authCheck, async function(req: any, res: any) {
	const comment = new Comment();
	comment.text = DOMPurify.sanitize(req.body.text);
	comment.creator = req.body.creator;
	comment.dateCreated = req.body.created;
	await commentService.createComment(comment, parseInt(req.params.taskId), req.user);
	res.send();
});
commentsRouter.put('/:commentId', Comment.validators(), validationMiddleware, authCheck, async function(
	req: Request<{ taskId: string; commentId: string }>,
	res: any
) {
	const comment = req.body;
	comment.text = DOMPurify.sanitize(req.body.text);
	await commentService.editComment(comment, parseInt(req.params.commentId));
	res.send();
});

commentsRouter.delete('/:commentId', authCheck, async function(
	req: Request<{ taskId: string; commentId: string }>,
	res: any
) {
	await commentService.removeComment(parseInt(req.params.commentId));
	res.send();
});

export default commentsRouter;
