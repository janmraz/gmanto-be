import { Request } from 'express';
import authCheck from '../auth/authChecker';
import Comment from '../entity/Comment';
import commentService from '../services/CommentService';
import validationMiddleware from '../core/validationMiddleware';
import express from 'express';
import User from '../entity/User';

const notificationsRouter = express.Router({ mergeParams: true });

notificationsRouter.get('/', authCheck, async function(req: Request<{ projectId: string }>, res: any) {
	const skip = req.query.skip;
	const take = req.query.take;
	const notifications = await commentService.getInfoComments(
		parseInt(req.params.projectId),
		req.user as User,
		skip,
		take
	);
	res.send(notifications);
});

notificationsRouter.post('/', Comment.validators(), validationMiddleware, authCheck, async function(
	req: Request<{ projectId: string }>,
	res: any
) {
	await commentService.removeNotifications(req.body.commentIds, req.user as User);
	res.send();
});

export default notificationsRouter;
