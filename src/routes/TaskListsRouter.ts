import { Request } from 'express';
import taskListService from '../services/TaskListService';
import TaskList from '../entity/TaskList';
import authCheck from '../auth/authChecker';
import validationMiddleware from '../core/validationMiddleware';
import express from 'express';
import User from '../entity/User';

const taskListRouter = express.Router({ mergeParams: true });

taskListRouter.get('/', authCheck, async function(req: Request<{ projectId: string }>, res: any, next: any) {
	const projects = await taskListService.getAllTaskLists(parseInt(req.params.projectId), req.user as User);
	res.send(projects);
});

taskListRouter.get('/:taskListId', authCheck, async function(
	req: Request<{ projectId: string; taskListId: string }>,
	res: any,
	next: any
) {
	const projects = await taskListService.getTaskList(parseInt(req.params.taskListId));
	res.send(projects);
});

taskListRouter.post('/', TaskList.validators(), validationMiddleware, authCheck, async function(req: any, res: any) {
	const taskList = new TaskList();
	taskList.name = req.body.name;
	taskList.archived = false;
	await taskListService.createTaskList(taskList, parseInt(req.params.projectId), req.user as User);
	res.send();
});

taskListRouter.post('/:taskListId/change-order', authCheck, async function(req: any, res: any, next: any) {
	const order = req.body.order;
	await taskListService.changePosition(
		parseInt(req.params.projectId),
		parseInt(req.params.taskListId),
		parseInt(req.user.id),
		parseInt(order)
	);
	res.send();
});

taskListRouter.put('/:taskListId', TaskList.validators(), validationMiddleware, authCheck, async function(
	req: Request<{ taskListId: string; projectId: string }>,
	res: any
) {
	const taskList = await taskListService.getTaskList(parseInt(req.params.taskListId));
	taskList.name = req.body.name;
	taskList.archived = req.body.archived;
	await taskListService.saveTaskList(taskList);
	res.send();
});

taskListRouter.delete('/:taskListId', authCheck, async function(
	req: Request<{ taskListId: string; projectId: string }>,
	res: any
) {
	const taskList = await taskListService.getTaskList(parseInt(req.params.taskListId));
	await taskListService.removeTaskList(taskList);
	res.send();
});

export default taskListRouter;
