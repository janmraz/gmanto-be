import express from 'express';
const debugRouter = express.Router();

/* GET status health-check. */
debugRouter.get('/', function(req, res, next) {
	res.send({ app: 'ok' });
});

export default debugRouter;
