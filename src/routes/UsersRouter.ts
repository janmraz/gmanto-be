import { getConnection } from 'typeorm';
import User from '../entity/User';
import { Request } from 'express';
import authCheck from '../auth/authChecker';
import express from 'express';
import uploadFile, {deleteFile} from '../core/S3Uploader';
import multer from 'multer';
import logger from '../core/logger';
import cropImage from "../core/ImageTransformer";

const upload = multer({ dest: 'uploads/' });

const UserRouter = express.Router();

const repository = () => getConnection().getRepository(User);

UserRouter.get('/', authCheck, async function(req: any, res: any, next: any) {
	const users = await repository().find();
	res.send(users);
});

UserRouter.get('/logout', authCheck, async function(req: any, res: any, next: any) {
	repository()
		.findOne(req.user.id)
		.then(user => {
			user.token = '';
			repository()
				.save(user)
				.then(() => res.send());
		});
});

UserRouter.get('/user/:id', authCheck, async function(req: Request<{ id: string }>, res: any, next: any) {
	const user = await repository().findOne(req.params.id);
	res.send(user);
});

UserRouter.post('/user/:id', authCheck, async function(req: Request<{ id: string }>, res: any, next: any) {
	const user = await repository().findOne(req.params.id);
	user.firstName = req.body.firstName;
	user.lastName = req.body.lastName;
	user.darkMode = req.body.darkMode;
	user.notificationOn = req.body.notificationOn;
	user.showCommentsOnly = req.body.showCommentsOnly;
	user.showAllNotification = req.body.showAllNotification;
	await repository().save(user);
	res.send();
});

UserRouter.post('/user/:id/upload-picture', authCheck, upload.single('avatar'), async function(
	req: Request<{ id: string }>,
	res: any,
	next: any
) {
	const user = await repository().findOne(req.params.id);
	const path = await cropImage(req.file.path);
	user.profileImgUrl = await uploadFile(req.file.originalname, path, "profile_pictures", user.id.toString());
	await repository().save(user);
	res.send();
});

UserRouter.post('/user/:id/remove-picture', authCheck, async function(
	req: Request<{ id: string }>,
	res: any,
	next: any
) {
	const user = await repository().findOne(req.params.id);
	if(user.profileImgUrl.includes('s3.amazonaws.com/')){
		await deleteFile(user.profileImgUrl);
	}
	user.profileImgUrl = "";
	await repository().save(user);
	res.send();
});

UserRouter.delete('/user/:id', authCheck, async function(req: Request<{ id: string }>, res: any, next: any) {
	const user = await repository().findOne(req.params.id);
	await repository().remove(user);
	res.send();
});

UserRouter.get('/profile', authCheck, async function(req: Request<{ id: string }>, res: any, next: any) {
	return res.send(req.user);
});

export default UserRouter;
