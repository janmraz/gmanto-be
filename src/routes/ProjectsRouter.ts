import { Request } from 'express';
import Project from '../entity/Project';
import projectService from '../services/ProjectService';
import authCheck from '../auth/authChecker';
import validationMiddleware from '../core/validationMiddleware';

import express from 'express';
import AccessDenied from '../core/AccessDenied';
import logger from '../core/logger';
import mailer from '../core/mail/mail';
const projectsRouter = express.Router();

projectsRouter.get('/', authCheck, async function(req: any, res: any, next: any) {
	const projects = await projectService.getAllProjects(req.user.id);
	res.send(projects);
});

projectsRouter.get('/:id', authCheck, async function(req: any, res: any, next: any) {
	try {
		const project = await projectService.getProjectById(parseInt(req.params.id), req.user.id);
		res.send(project);
	} catch (e) {
		console.error(e);
		if (e instanceof AccessDenied) {
			res.statusCode = 401;
			res.send({ accessDenied: true });
		}
	}
});

projectsRouter.post('/', Project.validators(), validationMiddleware, authCheck, async function(
	req: Request,
	res: any,
	next: any
) {
	const project = new Project();
	project.name = req.body.name;
	await projectService.createProject(project,req.body.creator );
	res.send();
});

projectsRouter.post('/:id/invite', authCheck, async function(req: Request<{ id: string }>, res: any, next: any) {
	const email = req.body.email;
	const invite = await projectService.addCollaboratorToProject(req.params.id, email);
	res.send({invite});
});

projectsRouter.post('/:id/change-order', authCheck, async function(req: any, res: any, next: any) {
	const order = req.body.order;
	await projectService.changePosition(parseInt(req.params.id), parseInt(req.user.id), parseInt(order));
	res.send();
});

projectsRouter.post('/:id/remove-collaborator', authCheck, async function(
	req: Request<{ id: string }>,
	res: any,
	next: any
) {
	const user = req.body.user;
	await projectService.removeCollaboratorToProject(req.params.id, user);
	res.send();
});

projectsRouter.post('/:id/change-owner', authCheck, async function(req: Request<{ id: string }>, res: any, next: any) {
	const owner = req.body.user;
	await projectService.changeOwnerProject(req.params.id, owner);
	res.send();
});

projectsRouter.post('/:id/request-access', authCheck, async function(req: any, res: any, next: any) {
	logger.debug('send request acces to ' + req.body.user.email);
	const project = await projectService.getProjectById(parseInt(req.params.id), req.user.id);
	await mailer.sendRequestAccessEmail(project, req.body.user.email);
	res.send();
});

projectsRouter.put('/:id', Project.validators(), validationMiddleware, authCheck, async function(
	req: any,
	res: any,
	next: any
) {
	const project = await projectService.getProjectById(parseInt(req.params.id), req.user.id);
	project.name = req.body.name;
	project.archived = req.body.archived;
	await projectService.editProject(project);
	res.send();
});

projectsRouter.delete('/:id', authCheck, async function(req: any, res: any, next: any) {
	const project = await projectService.getProjectById(parseInt(req.params.id), req.user.id);
	await projectService.removeProject(project);
	res.send();
});

export default projectsRouter;
