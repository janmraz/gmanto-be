import { Request } from 'express';
import Task from '../entity/Task';
import taskService from '../services/TaskService';
import authCheck from '../auth/authChecker';
import validationMiddleware from '../core/validationMiddleware';
import express from 'express';
import multer from 'multer';
import uploadFile from '../core/S3Uploader';
import User from '../entity/User';

const upload = multer({ dest: 'uploads/' });

const tasksRouter = express.Router({ mergeParams: true });

tasksRouter.post('/', Task.validators(), validationMiddleware, authCheck, async function(req: any, res: any) {
	const task = new Task();
	task.name = req.body.name;
	task.done = false;
	task.followers = req.body.followers;
	await taskService.createTask(task, parseInt(req.params.taskListId), req.user);
	res.send();
});

tasksRouter.get('/:taskId', authCheck, async function(req: Request<{ taskId: string }>, res: any) {
	const task = await taskService.getTask(parseInt(req.params.taskId));
	res.send(task);
});

tasksRouter.post('/:taskId/rename', authCheck, async function(req: Request<{ taskId: string }>, res: any) {
	const task = await taskService.renameTask(parseInt(req.params.taskId), req.body.name, req.user as User);
	res.send(task);
});

tasksRouter.post('/:taskId/add-follower', authCheck, async function(req: Request<{ taskId: string }>, res: any) {
	const task = await taskService.addFollower(parseInt(req.params.taskId), req.body.user);
	res.send(task);
});

tasksRouter.post('/:taskId/remove-follower', authCheck, async function(req: Request<{ taskId: string }>, res: any) {
	const task = await taskService.removeFollower(parseInt(req.params.taskId), req.body.user);
	res.send(task);
});

tasksRouter.post('/:taskId/done', authCheck, async function(req: any, res: any) {
	await taskService.toggleTask(parseInt(req.params.taskId), true, req.user);
	res.send();
});

tasksRouter.post('/:taskId/undone', authCheck, async function(req: any, res: any) {
	await taskService.toggleTask(parseInt(req.params.taskId), false, req.user);
	res.send();
});

tasksRouter.post('/:taskId/change-deadline', authCheck, async function(req: any, res: any) {
	await taskService.changeDateDue(parseInt(req.params.taskId), req.body.deadline, req.user);
	res.send();
});

tasksRouter.post('/:taskId/change-assignee', authCheck, async function(req: any, res: any) {
	await taskService.changeAssignee(parseInt(req.params.taskId), req.body.assignee, req.user);
	res.send();
});

tasksRouter.post('/:taskId/upload-file', upload.single('file'), async function(
	req: Request<{ taskId: string; taskListId: string }>,
	res: any
) {
	const projectId = await taskService.getTaskProjectId(parseInt(req.params.taskId));
	const destination = await uploadFile(req.file.originalname, req.file.path, projectId, req.params.taskId);
	res.send(destination);
});

tasksRouter.post('/:taskId/change-order', authCheck, async function(req: any, res: any) {
	await taskService.changePosition(
		parseInt(req.params.taskListId),
		parseInt(req.params.taskId),
		parseInt(req.body.order)
	);
	res.send();
});

tasksRouter.post('/:taskId/move-task', authCheck, async function(req: any, res: any) {
	console.log('move', parseInt(req.params.taskListId), parseInt(req.params.taskId), parseInt(req.body.order));
	await taskService.moveTask(parseInt(req.params.taskListId), parseInt(req.params.taskId), parseInt(req.body.order));
	res.send();
});

tasksRouter.delete('/:taskId', authCheck, async function(req: Request<{ taskId: string }>, res: any) {
	const task = await taskService.removeTask(parseInt(req.params.taskId));
	res.send(task);
});

export default tasksRouter;
