import { getConnection } from 'typeorm';
import { OAuth2Strategy as GoogleStrategy } from 'passport-google-oauth';
import User from '../entity/User';
import passport from 'passport';
import config from "../config";

const repository = () => getConnection().getRepository(User);

passport.use(
	new GoogleStrategy(
		{
			clientID: '729734224079-r0243q2di61r5ae3uaa4gtorvn718tca.apps.googleusercontent.com',
			clientSecret: 'zfSRj1rzcKRI4QfeyEEt9V3g',
			callbackURL: config.backendUrl + '/auth/google/callback',
		},
		function(accessToken, refreshToken, profile, done) {
			repository()
				.find({ email: profile.emails[0].value })
				.then(async result => {
					const user = result.length === 0 ? new User() : result[0];
					user.firstName = profile.name.givenName;
					user.lastName = profile.name.familyName;
					user.googleId = profile.id;
					user.token = accessToken;
					user.email = profile.emails[0].value;
					if(user.profileImgUrl === undefined && profile.photos.length > 0){
						user.profileImgUrl = profile.photos[0].value;
					}
					if(!user.dateCreated){
						user.dateCreated = new Date();
					}
					user.registered = true;
					repository()
						.save(user)
						.then(result => {
							done(null, result);
						});
				});
		}
	)
);

passport.serializeUser(function(user, done) {
	done(null, user);
});

passport.deserializeUser(function(user, done) {
	done(null, user);
});

export default passport;
