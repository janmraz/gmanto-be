import { getConnection } from 'typeorm';
import User from '../entity/User';
import logger from '../core/logger';

const authCheck = async (req, res, next) => {
	const token = req.headers.authorization ? req.headers.authorization.split(' ')[1] : 'nonce';
	// fetch user from db based on given token
	const users = await getConnection()
		.getRepository(User)
		.find({ token });

	// not authenticated - return 403
	if (users.length === 0) {
		res.statusCode = 401;
		logger.debug('auth token', token);
		return res.send({ Authentication: 'not-logged-in' });
	}

	req.user = users[0];

	// succesfully authenticated
	return next();
};

export default authCheck;
