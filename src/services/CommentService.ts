import { getConnection, In } from 'typeorm';
import Comment from '../entity/Comment';
import Task from '../entity/Task';
import io from '../app';
import Notification from '../entity/Notification';
import User from '../entity/User';
import mailer from '../core/mail/mail';

class CommentService {
	repository = () => getConnection().getRepository(Comment);
	taskRepository = () => getConnection().getRepository(Task);
	userRepository = () => getConnection().getRepository(User);

	createComment = async (comment: Comment, taskId: number, user: User): Promise<void> => {
		const task = await this.taskRepository().findOne(taskId, {
			relations: ['taskList', 'taskList.project'],
		});

		comment.dateModified = new Date();
		comment.dateCreated = new Date();
		comment.notifications = this.createNotifications(comment, task);
		task.comments.push(comment);
		const mentionIds = this.getMentions(comment.text);
		const mentionedUsers = await this.userRepository().findByIds(mentionIds);
		const notUsedMentioned = task.followers.filter(user => mentionedUsers.indexOf(user) > -1);
		notUsedMentioned.map(user => {
			const notification = new Notification();
			notification.recipient = user;
			notification.comment = comment;
			comment.notifications.push(notification);
		});

		// get email notifications
		const emailNotificationUsers = notUsedMentioned.concat(task.followers);
		Promise.all(
			emailNotificationUsers.map(follower =>
				mailer.sendMentionsEmail(user, follower, task, task.taskList.project.id.toString())
			)
		);

		await this.taskRepository().save(task);
		io.emit('data', { taskId });
		return;
	};

	editComment = async (comment: Comment, commentId: number): Promise<void> => {
		const storedComment = await this.getComment(commentId);
		storedComment.text = comment.text;
		storedComment.dateModified = new Date();
		return this.saveComment(storedComment);
	};

	getComment = async (commentId: number): Promise<Comment> => {
		return this.repository().findOne(commentId);
	};

	getInfoComments = async (projectId: number, user: User, skip: number, take: number): Promise<Comment[]> => {
		const comments = await this.repository().find({
			order: { dateCreated: 'DESC' },
			skip,
			take,
			relations: ['task', 'task.taskList', 'task.taskList.project', 'notifications', 'notifications.recipient'],
			where: { 'task.taskList.project': { id: projectId } }, // FIXME doesnt work right now for some reason
		});
		return comments
			.filter(c => c.task.taskList.project.id === projectId) // there is fix for right now
			.map(comment => {
				if (comment.notifications.find(not => not.recipient.id === user.id)) {
					comment.seen = false;
					return comment;
				}
				comment.seen = true;
				return comment;
			});
	};

	removeNotifications = async (commentIds: number[], user: User): Promise<void> => {
		const notifications = await getConnection()
			.getRepository(Notification)
			.find({
				relations: ['comment', 'recipient'],
				where: { comment: { id: In(commentIds) }, recipient: { id: user.id } },
			});
		await getConnection()
			.getRepository(Notification)
			.remove(notifications);
	};

	saveComment = async (comment: Comment): Promise<void> => {
		await this.repository().save(comment);
	};

	removeComment = async (commentId: number) => {
		const comment = await this.getComment(commentId);
		await this.repository().remove(comment);
	};

	createNotifications = (comment: Comment, task: Task): Notification[] => {
		const notifications = [];
		task.followers.map(follower => {
			const notification = new Notification();
			notification.recipient = follower;
			notification.comment = comment;
			notifications.push(notification);
		});
		return notifications;
	};

	getMentions = (text: string): string[] => {
		const pattern = /\B@[a-z0-9_.-]+/gi;
		const mentions = text.match(pattern);
		if (!mentions) {
			return [];
		}
		console.log('mentioned', mentions);
		return mentions.map(mention => mention.split('@')[1].split('.')[2]);
	};
}

const commentService = new CommentService();
export default commentService;
