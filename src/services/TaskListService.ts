import Project from '../entity/Project';
import { getConnection } from 'typeorm';
import TaskList from '../entity/TaskList';
import User from '../entity/User';
import Notification from '../entity/Notification';

class TaskListService {
	repository = () => getConnection().getRepository(TaskList);
	projectRepository = () => getConnection().getRepository(Project);
	notificationRepository = () => getConnection().getRepository(Notification);

	createTaskList = async (taskList: TaskList, projectId: number, user: User): Promise<void> => {
		taskList.dateCreated = new Date();
		taskList.creator = user;
		taskList.project = await this.projectRepository().findOne(projectId);
		return this.saveTaskList(taskList);
	};

	getTaskList = async (taskListId: number): Promise<TaskList> => {
		return this.repository().findOne(taskListId);
	};

	getAllTaskLists = async (projectId: number, user: User): Promise<TaskList[]> => {
		const project = await this.projectRepository().findOne(projectId);
		const taskLists = await this.repository().find({ project });
		const notSeen = await this.notificationRepository().find({
			relations: ['comment'],
			where: { recipient: user },
		});
		const commentIds = notSeen.map(not => not.comment.id);
		return taskLists.map(taskList => {
			taskList.tasks.map(task => {
				task.comments.map(comment => {
					comment.seen = commentIds.findIndex(c => c === comment.id) === -1;
					return comment;
				});
				return task;
			});
			return taskList;
		});
	};

	changePosition = async (projectId: number, taskListId: number, userId: number, order: number) => {
		const project = await this.projectRepository().findOne(projectId);
		const taskLists = await this.repository().find({ project });

		const currentArr = taskLists.sort((a, b) => a.order - b.order);
		const originalPosition = currentArr.findIndex(t => t.id === taskListId);

		const updatedTaskLists = this.reorderTaskLists({ newIndex: order, oldIndex: originalPosition }, currentArr);

		await this.repository().save(updatedTaskLists);
	};

	reorderTaskLists = (event: { newIndex: number; oldIndex: number }, originalArray: TaskList[]) => {
		const movedItem = originalArray.filter((item, index) => index === event.oldIndex);
		const remainingItems = originalArray.filter((item, index) => index !== event.oldIndex);

		const reorderedItems = [
			...remainingItems.slice(0, event.newIndex),
			movedItem[0],
			...remainingItems.slice(event.newIndex),
		];

		return reorderedItems.map((c, i) => {
			c.order = i;
			return c;
		});
	};

	archiveTaskList = async (taskListId: number) => {
		const taskList: TaskList = await this.repository().findOne(taskListId);
		taskList.archived = true;
		this.saveTaskList(taskList);
	};

	removeTaskList = async (taskList: TaskList) => {
		await this.repository().remove(taskList);
	};

	saveTaskList = async (taskList: TaskList): Promise<void> => {
		await this.repository().save(taskList);
	};
}

const taskListService = new TaskListService();
export default taskListService;
