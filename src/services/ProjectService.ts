import Project from '../entity/Project';
import { getConnection } from 'typeorm';
import User from '../entity/User';
import AccessDenied from '../core/AccessDenied';
import { ProjectToUser } from '../entity/ProjectToUser';

class ProjectService {
	repository = () => getConnection().getRepository(Project);
	userRepository = () => getConnection().getRepository(User);
	projectToUserRepository = () => getConnection().getRepository(ProjectToUser);

	createProject = async (project: Project, user: User): Promise<void> => {
		project.dateCreated = new Date();
		const projectToUser = new ProjectToUser();
		projectToUser.owner = true;
		projectToUser.user = user;
		projectToUser.project = project;
		project.collaborators = [projectToUser];
		return this.saveProject(project);
	};

	getAllProjects = async (userId: number): Promise<Project[]> => {
		const projects = await this.repository().find({ relations: ['taskLists', 'taskLists.tasks'] });
		const usersProjects = projects.filter(p => p.collaborators.some(c => c.user.id === userId));

		// sum up all open tasks and assigned to user
		return usersProjects.map(project => {
			const sum: ProjectDTOSum = project.taskLists
				.map(list => {
					const tasksOpen = list.tasks.map(task => (task.done ? 0 : 1)).reduce((a, b) => a + b, 0);
					const tasksMine = list.tasks
						.map(task => (!task.done && task.assignee && task.assignee.id === userId ? 1 : 0))
						.reduce((a, b) => a + b, 0);
					return { tasksOpen, tasksMine };
				})
				.reduce((a, b) => ({ tasksOpen: a.tasksOpen + b.tasksOpen, tasksMine: a.tasksMine + b.tasksMine }), {
					tasksOpen: 0,
					tasksMine: 0,
				});
			project.tasksOpen = sum.tasksOpen;
			project.tasksMine = sum.tasksMine;
			return project;
		});
	};

	getProjectById = async (id: number, userId: number): Promise<Project> => {
		const project = await this.repository().findOne(id);
		if (project.collaborators.some(c => c.user.id === userId)) {
			return project;
		}
		throw new AccessDenied();
	};

	addCollaboratorToProject = async (projectId: string, email: string): Promise<boolean> => {
		let user = await this.userRepository().findOne({ email: email });
		const project = await this.repository().findOne(projectId);
		let newInvite = false;

		// if user is not present then create new one
		if (!user) {
			user = new User();
			newInvite = true;

			user.email = email;
			user.googleId = '';
			user.token = '';
			user.firstName = '';
			user.lastName = '';
			user.collaborators = [];
			await this.userRepository().save(user);
			user = await this.userRepository().findOne({ email: email });
		}
		if (!project.collaborators) {
			project.collaborators = [];
		}
		if (project.collaborators.findIndex(c => c.user.id === user.id && c.project.id === project.id) < 0) {
			const projectToUser = new ProjectToUser();
			projectToUser.project = project;
			projectToUser.user = user;
			project.collaborators.push(projectToUser);
		}

		await this.saveProject(project);
		return newInvite;
	};
	removeCollaboratorToProject = async (projectId: string, user: User): Promise<void> => {
		const project = await this.repository().findOne(projectId);
		project.collaborators = project.collaborators.filter(u => u.user.id !== user.id);
		this.saveProject(project);
	};
	changeOwnerProject = async (projectId: string, user: User): Promise<void> => {
		const project = await this.repository().findOne(projectId);
		const owner = project.collaborators.find(u => u.owner);
		const newOwner = project.collaborators.find(u => u.user.id === user.id);
		project.collaborators = project.collaborators.map(p => {
			if (p.id === owner.id) {
				p.owner = false;
			}
			if (p.id === newOwner.id) {
				p.owner = true;
			}
			return p;
		});
		this.saveProject(project);
	};

	changePosition = async (id: number, userId: number, order: number) => {
		const projectToUsers = await this.projectToUserRepository().find({
			relations: ['project'],
			where: { user: { id: userId } },
		});

		const currentArr = projectToUsers.sort((a, b) => a.order - b.order);
		const originalPosition = currentArr.findIndex(c => c.project.id === id);

		const updatedProjectsToUsers = this.reorderProjectsToUser(
			{ newIndex: order, oldIndex: originalPosition },
			currentArr
		);
		await this.projectToUserRepository().save(updatedProjectsToUsers);
	};

	reorderProjectsToUser = (event: { newIndex: number; oldIndex: number }, originalArray: ProjectToUser[]) => {
		const movedItem = originalArray.filter((item, index) => index === event.oldIndex);
		const remainingItems = originalArray.filter((item, index) => index !== event.oldIndex);

		const reorderedItems = [
			...remainingItems.slice(0, event.newIndex),
			movedItem[0],
			...remainingItems.slice(event.newIndex),
		];

		return reorderedItems.map((c, i) => {
			c.order = i;
			return c;
		});
	};

	editProject = async (project: Project): Promise<void> => {
		return this.saveProject(project);
	};

	removeProject = async (project: Project): Promise<void> => {
		return this.repository()
			.remove(project)
			.then(() => {});
	};

	saveProject = async (project: Project): Promise<void> => {
		await this.repository().save(project);
	};
}

interface ProjectDTOSum {
	tasksOpen: number;
	tasksMine: number;
}

const projectService = new ProjectService();
export default projectService;
