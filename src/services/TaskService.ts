import { getConnection } from 'typeorm';
import TaskList from '../entity/TaskList';
import Task from '../entity/Task';
import User from '../entity/User';
import Notification from '../entity/Notification';
import Comment from '../entity/Comment';
import mailer from '../core/mail/mail';
import logger from '../core/logger';
const { DateTime } = require('luxon');

class TaskService {
	taskListRepository = () => getConnection().getRepository(TaskList);
	repository = () => getConnection().getRepository(Task);

	createTask = async (task: Task, taskListId: number, user: User): Promise<void> => {
		task.taskList = await this.taskListRepository().findOne(taskListId, { relations: ['project'] });
		task.dateCreated = new Date();
		task.creator = user;

		// create comment and notifications
		const comment = new Comment();
		comment.dateCreated = new Date();
		comment.dateModified = new Date();
		comment.creator = user;
		comment.infoComment = true;
		comment.task = task;
		comment.text = user.getFullName() + ' has created task ' + task.name;
		comment.notifications = this.createNotifications(comment, task);

		task.comments = [];
		task.comments.push(comment);

		return this.saveTask(task);
	};

	getTask = async (taskId: number): Promise<Task> => {
		return this.repository().findOne(taskId, { relations: ['taskList'] });
	};

	getTaskProjectId = async (taskId: number): Promise<string> => {
		const task = await this.repository().findOne(taskId, { relations: ['taskList', 'taskList.project'] });
		return task.taskList.project.id.toString();
	};

	toggleTask = async (taskId: number, value: boolean, user: User) => {
		const task: Task = await this.getTask(taskId);

		// create comment and notifications
		const comment = new Comment();
		comment.dateCreated = new Date();
		comment.dateModified = new Date();
		comment.creator = user;
		comment.infoComment = true;
		comment.task = task;
		comment.text = value
			? user.getFullName() + ' has completed task ' + task.name
			: user.getFullName() + ' has reopened task ' + task.name;
		comment.notifications = this.createNotifications(comment, task);

		// send email notifications
		if (value) {
			this.getTaskProjectId(task.id)
				.then(projectId =>
					Promise.all(
						task.followers.map(follower => mailer.sendTaskCompletionEmail(user, follower, task, projectId))
					)
				)
				.catch(e => logger.error(e));
		}

		task.comments.push(comment);

		task.done = value;
		return this.saveTask(task);
	};

	renameTask = async (taskId: number, value: string, user: User) => {
		const task: Task = await this.getTask(taskId);

		// create comment and notifications
		const comment = new Comment();
		comment.dateCreated = new Date();
		comment.dateModified = new Date();
		comment.creator = user;
		comment.infoComment = true;
		comment.task = task;
		comment.text = user.getFullName() + ' has renamed task' + task.name + 'from ' + task.name + ' to ' + value;
		comment.notifications = this.createNotifications(comment, task);

		task.comments.push(comment);
		task.name = value;
		this.saveTask(task);
	};

	changeDateDue = async (taskId: number, dateDue: Date, user: User) => {
		const task: Task = await this.getTask(taskId);

		// create comment and notifications
		const comment = new Comment();
		comment.dateCreated = new Date();
		comment.dateModified = new Date();
		comment.creator = user;
		comment.infoComment = true;
		comment.task = task;
		if (task.dateDue) {
			comment.text =
				user.getFullName() +
				' has changed dateDue of task ' +
				task.name +
				' from ' +
				DateTime.fromJSDate(task.dateDue).toISODate() +
				' to ' +
				DateTime.fromISO(dateDue).toISODate();
		} else {
			comment.text =
				user.getFullName() +
				' has changed dateDue of task ' +
				task.name +
				' to ' +
				DateTime.fromISO(dateDue).toISODate();
		}
		comment.notifications = this.createNotifications(comment, task);
		task.comments.push(comment);

		task.dateDue = dateDue;
		return this.saveTask(task);
	};

	changeAssignee = async (taskId: number, newAssignee: User | null, user: User) => {
		const task: Task = await this.getTask(taskId);

		// create comment and notifications
		const comment = new Comment();
		comment.dateCreated = new Date();
		comment.dateModified = new Date();
		comment.creator = user;
		comment.infoComment = true;
		comment.task = task;
		if (task.assignee) {
			const newAssigneeString = newAssignee !== null ? newAssignee.getFullName() : 'Unassigned';
			comment.text =
				user.getFullName() +
				' has changed assignee of task ' +
				task.name +
				' from ' +
				task.assignee.getFullName() +
				' to ' +
				newAssigneeString;
		} else {
			comment.text =
				user.getFullName() +
				' has set assignee of task ' +
				task.name +
				'to ' +
				newAssignee.firstName +
				' ' +
				newAssignee.lastName;
		}
		comment.notifications = this.createNotifications(comment, task);
		task.comments.push(comment);

		task.assignee = newAssignee;
		if (newAssignee !== null && !task.followers.find(u => u.id === newAssignee.id)) {
			task.followers.push(newAssignee);
		}
		return this.saveTask(task);
	};

	moveTask = async (taskListId: number, taskId: number, order: number) => {
		const task: Task = await this.getTask(taskId);
		task.taskList = await this.taskListRepository().findOne(taskListId, { relations: ['project'] });

		await this.repository().save(task);
		await this.changePosition(taskListId, taskId, order);
	};

	changePosition = async (taskListId: number, taskId: number, order: number) => {
		const taskList = await this.taskListRepository().findOne(taskListId);
		const tasks = await this.repository().find({ taskList });

		const currentArr = tasks.sort((a, b) => a.order - b.order);
		const originalPosition = currentArr.findIndex(t => t.id === taskId);

		const updatedTasks = this.reorderTasks({ newIndex: order, oldIndex: originalPosition }, currentArr);

		await this.repository().save(updatedTasks);
	};

	reorderTasks = (event: { newIndex: number; oldIndex: number }, originalArray: Task[]) => {
		const movedItem = originalArray.filter((item, index) => index === event.oldIndex);
		const remainingItems = originalArray.filter((item, index) => index !== event.oldIndex);

		const reorderedItems = [
			...remainingItems.slice(0, event.newIndex),
			movedItem[0],
			...remainingItems.slice(event.newIndex),
		];

		return reorderedItems.map((c, i) => {
			c.order = i;
			return c;
		});
	};

	removeFollower = async (taskId: number, user: User): Promise<void> => {
		const task = await this.getTask(taskId);
		task.followers = task.followers.filter(u => u.id !== user.id);
		this.saveTask(task);
	};

	addFollower = async (taskId: number, user: User): Promise<void> => {
		const task = await this.getTask(taskId);
		task.followers.push(user);
		this.saveTask(task);
	};

	saveTask = async (task: Task): Promise<void> => {
		await this.repository().save(task);
	};
	removeTask = async (taskId: number) => {
		const task = await this.getTask(taskId);
		await this.repository().remove(task);
	};

	createNotifications = (comment: Comment, task: Task): Notification[] => {
		const notifications = [];
		task.followers.map(follower => {
			const notification = new Notification();
			notification.recipient = follower;
			notification.comment = comment;
			notifications.push(notification);
		});
		return notifications;
	};
}

const taskService = new TaskService();
export default taskService;
