export default {
	backendUrl: process.env.NODE_ENV === 'development' ? 'http://localhost:5000' : 'https://gmanto-be.herokuapp.com',
    frontendUrl:
		process.env.NODE_ENV === 'development'
			? 'http://localhost:3000'
			: 'https://f-merge-final.d3mik7ehxs4y9f.amplifyapp.com',
};
