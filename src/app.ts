import { Socket } from 'socket.io';

import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import cors from 'cors';

import 'express-async-errors';
import 'reflect-metadata';

import { createConnection } from 'typeorm';
import passport from './auth/passport';

import debugRouter from './routes/DebugRouter';
import usersRouter from './routes/UsersRouter';
import taskListRouter from './routes/TaskListsRouter';
import tasksRouter from './routes/TasksRouter';
import commentsRouter from './routes/CommentsRouter';
import projectsRouter from './routes/ProjectsRouter';
import logger from './core/logger';
import notificationsRouter from './routes/NotificationRouter';
import config from "./config";

const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());

createConnection()
	.then(async connection => {
		// authentication routes
		app.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }));

		app.get(
			'/auth/google/callback',
			passport.authenticate('google', { failureRedirect: config.frontendUrl + '/welcome', session: false }),
			async (req, res) => {
				const token = req.user['token'];
				logger.debug('user' + req.user);
				res.redirect(config.frontendUrl + '?token=' + token);
			}
		);

		// register all routers
		app.use('/', debugRouter);
		app.use('/users', usersRouter);
		app.use('/projects', projectsRouter);
		app.use('/projects/:projectId/notifications', notificationsRouter);
		app.use('/projects/:projectId/taskLists', taskListRouter);
		app.use('/taskLists/:taskListId/tasks', tasksRouter);
		app.use('/tasks/:taskId/comments', commentsRouter);

		io.on('connection', (socket: Socket) => {
			logger.silly('user has connected');

			socket.on('disconnect', () => logger.silly('Client disconnected'));
		});

		const port = process.env.PORT || 5000;
		http.listen(port, () => {
			logger.debug('Gmanto app listening on port ' + port);
		});
	})
	.catch(e => logger.error('err ' + e));

export default io;
